import React from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    TouchableOpacityBase,
    StatusBar,TextInput, TouchableOpacity, Button, ToastAndroid, Alert,FlatList
  } from 'react-native';
import { event } from 'react-native-reanimated';
class ListData extends React.Component 
{
    constructor(props) {
        super(props);
        this.state = {
          dataSource:[],
          query:null,
          dataBackup:[],
          obj:null
         };
       }
       componentDidMount(){
        fetch('http://192.168.43.121:80/demo/api.php?type1=getData', {
          method: 'get',
          header:{
            'Accept':'application/json',
            'Content-type':'application/json'
          }
       })
       .then((response) => response.json())
       .then((responseJson) => {
          //var obj = JSON.parse(response);
          console.log(responseJson);
          this.setState({
    
            dataSource:responseJson['data'],
            dataBackup:responseJson['data']
          })
          
          //Alert.alert(""+responseJson['message'])
    
       })
       .catch((error) => {
          console.error(error);
       });  
      }
      deleteData=(id)=>
      {
        Alert.alert(
          "Delete",
          "Do you want to delete this record.",
          [
            {
              text: "Cancel",
              onPress: () => console.log("Cancel Pressed"),
              style: "cancel"
            },
            { text: "OK", onPress: () => {this.deleteCall(id)} }
          ],
          { cancelable: false
           }
        
        )}

      deleteCall(id)
      {
        fetch('http://192.168.43.121:80/demo/api.php?type1=deleteItem&id='+id, {
          method: 'get',
          header:{
            'Accept':'application/json',
            'Content-type':'application/json'
          }
       })
       .then((response) => response.json())
       .then((responseJson) => {
          //var obj = JSON.parse(response);
          if(responseJson['Action']=='1')
          {
            const filteredData = this.state.dataSource.filter(item => item.id !== id);
            this.setState({ dataSource: filteredData });
            this.setState({ dataBackup: filteredData });

            ToastAndroid.show("Delete data sucessfully.",ToastAndroid.LONG)
          }
          else
            ToastAndroid.show("problem to delete data.",ToastAndroid.LONG)
    
       })
       .catch((error) => {
          console.error(error);
       });  
      }
      
      filterItem=(inputText)=>{
      
        if(inputText=='')
        {
          this.setState({
            dataSource:this.state.dataBackup,
          });
        }
        else
        {
          var data=this.state.dataBackup;
          inputText=inputText.toLowerCase();
          data=data.filter(item => item.name.toLowerCase().match(inputText)||
          item.email.toLowerCase().match(inputText));
          this.setState({
            dataSource:data,
          });
        }

      };
      render(){
        return(
          <View>
           <TextInput
            // Adding hint in Text Input using Place holder.
            placeholder="Enter Text..." 
            // Making the Under line Transparent.
            underlineColorAndroid='transparent'
            onChangeText={this.filterItem}
            value={this.state.query}
            style={styles.TextInputStyleClass1}
  
          />
          
            <FlatList padding ={30}
             contentContainerStyle={{ paddingBottom: 80}}
             data={this.state.dataSource}
             renderItem={({item}) => 
             <TouchableOpacity onPress={this.deleteData.bind(this,item.id)}>
    
             <View style={styles.item} >
             <Text >Name:{item.name}</Text>
             <Text style={{height: 40}}>Email:{item.email}</Text>

             <Text style={{height: 40}} onPress={() =>
                this.props.navigation.navigate('EditData',item)}>edit</Text>

             <View style={{height: 1,backgroundColor:'gray'}}></View>
             </View>
             </TouchableOpacity>
            }
           />
          </View>
        );
      }
    }
    const styles = StyleSheet.create({
 
        MainContainer :{
         
        justifyContent: 'center',
        flex:1,
        margin: 10
        },
         
        TextInputStyleClass: {
         
        textAlign: 'center',
        marginBottom: 7,
        height: 40,
        borderWidth: 1,
        // Set border Hex Color Code Here.
         borderColor: '#2196F3',
         
         // Set border Radius.
         borderRadius: 5 ,
         
        // Set border Radius.
         //borderRadius: 10 ,
        },
        TextInputStyleClass1: {
        
          height: 40,
          borderRadius:10,
          backgroundColor:'#fff',
           // Set border Radius.
          
          }
         
        });
export default ListData;