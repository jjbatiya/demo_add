
import React from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    TouchableOpacityBase,
    StatusBar,TextInput, TouchableOpacity, Button, ToastAndroid, Alert,FlatList
  } from 'react-native';
class HomeScreen extends React.Component 
{
    constructor(props)
    {
      super(props)
      var myBoolean = false;
      this.state={
        userName:'',
        userEmail:'',
        userPassword:'',
        data:'',
        myBoolean:false
  
      }
     // console.warn("constructor called")
    }
    userRegister=()=>{
      const{userName}=this.state;
      const{userEmail}=this.state;
      const{userPassword}=this.state;
  
      if(true)
      {
      
      fetch('http://192.168.43.121:80/demo/api.php', {
        method: 'post',
        header:{
          'Accept':'application/json',
          'Content-type':'application/json'
        },
        body:JSON.stringify({
            type:"addData",
            name:userName,
            u_email:userEmail,
            u_password:userPassword
        })
     })
     .then((response) => response.json())
     .then((responseJson) => {
        //var obj = JSON.parse(response);
        console.log(responseJson['message']);
        Alert.alert(""+responseJson['message'])
  
     })
     .catch((error) => {
        console.error(error);
     });
    }
      
    }
     
    render()
    {
      //console.warn("render")
      return(
        <View style={styles.MainContainer}>
   
          <Text style= {{ fontSize: 20, color: "#000", textAlign: 'center', marginBottom: 15 }}>User Registration Form</Text>
    
          <TextInput
            
            placeholder="Enter User Name"
   
            onChangeText={userName => this.setState({userName})}
   
            // Making the Under line Transparent.
            underlineColorAndroid='transparent'
   
            style={styles.TextInputStyleClass}
          />
   
          <TextInput
            
            // Adding hint in Text Input using Place holder.
            placeholder="Enter User Email"
   
            onChangeText={userEmail => this.setState({userEmail})}
   
            // Making the Under line Transparent.
            underlineColorAndroid='transparent'
   
            style={styles.TextInputStyleClass}
          />
   
          <TextInput
            
            // Adding hint in Text Input using Place holder.
            placeholder="Enter User Password"
   
            onChangeText={userPassword => this.setState({userPassword})}
   
            // Making the Under line Transparent.
            underlineColorAndroid='transparent'
   
            style={styles.TextInputStyleClass}
   
            secureTextEntry={true}
          />
          <View style={{marginBottom:20}}>
          <Button title="Click Here To Register" onPress={this.userRegister} color="#2196F3" />
          </View>
          <View>
          <Button style={{height:40},{width:'40%'}} title="ListData" onPress={() =>
            this.props.navigation.navigate('ListUser')}/>
          </View>
    
   
  </View>
      );
    }
  }
// ...
const styles = StyleSheet.create({
 
    MainContainer :{
     
    justifyContent: 'center',
    flex:1,
    margin: 10
    },
     
    TextInputStyleClass: {
     
    textAlign: 'center',
    marginBottom: 7,
    height: 40,
    borderWidth: 1,
    // Set border Hex Color Code Here.
     borderColor: '#2196F3',
     
     // Set border Radius.
     borderRadius: 5 ,
     
    // Set border Radius.
     //borderRadius: 10 ,
    }
     
    });
export default HomeScreen;