/**
 * @format
 */
import 'react-native-gesture-handler'
import {AppRegistry} from 'react-native';
import {name as appName} from './app.json';
import Demo1 from './Demo1';

AppRegistry.registerComponent(appName, () => Demo1);
