import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import HomeScreen from './HomeScreen';
import ListData from './ListData';
import EditData from './EditData';

const Stack = createStackNavigator();

class Demo1 extends React.Component 
{
  //comment
  //demo test
  render() {
    return (
      <NavigationContainer>
        <Stack.Navigator>
          <Stack.Screen
            name="Home"
            component={HomeScreen}
          />
          <Stack.Screen
            name="ListUser"
            component={ListData}
          />
          <Stack.Screen 
            name="EditData"
            component={EditData}
          >

          </Stack.Screen>
        </Stack.Navigator>
      </NavigationContainer>
    );
  }
}

export default Demo1;